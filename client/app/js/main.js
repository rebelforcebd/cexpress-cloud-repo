var glat = 90.369006;
var glng = 23.755695;




mapboxgl.accessToken = 'pk.eyJ1IjoiY2V4cHJlc3MiLCJhIjoiY2puZTl4bXN3MTFkejNxbzc0MnJsYmRteCJ9.KC84SR25Ooy8DQ1FEr23Ng';
const map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/cexpress/cjnqa0fhn0vsw2rumsxz5oa9q',
center: [glat, glng],
zoom: 12.6,
pitch: 60

});

// Add geolocate control to the map.
map.addControl(new mapboxgl.GeolocateControl({
    positionOptions: {
        enableHighAccuracy: true
    },
    trackUserLocation: true
}));
