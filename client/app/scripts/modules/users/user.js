console.log('linked');
//http://localhost:3000/api/senders?access_token=5bb97a7f6d2caa1c91a580d&userID=%225bb97a7f6d2caa1c91a580d%22


$(document).ready(function() {
  var identifier = localStorage.getItem("identifier");
console.log(identifier)
  $.ajax({
    url: `https://cexpress.bubbleapps.io/version-test/api/1.1/obj/user/${identifier}`,
    type: 'GET',
  })
  .done(function(response) {
    var profileData = response.response;
    console.log(profileData);
    document.getElementById('content').innerHTML = profileTemplate(profileData);
    document.getElementById('thumbnail').innerHTML = profileImg(profileData);
    document.getElementById('delivery-counter').innerHTML = deliveryInfo(profileData);

  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });

});




function profileTemplate(profileData) {
return `
<h1>${profileData.Name}</h1>
<em>+880${profileData.Phone}</em>
<strong><i class="fa fa-map-marker"></i>${profileData.Address}</strong>
<em>${profileData.type}</em>
<a href="/store-history.html" class="button button-round button-green profile-page-button-1">History</a>
`
;}
function profileImg(profileData) {

  return `
  <img class="preload-image responsive-image" src="https:${profileData.profilePic}">
  `
}

function deliveryInfo(profileData) {
return `
<h1><strong>${profileData.deliveredItems}</strong><em>DELIVERED</em></h1>
<h1><strong>${profileData.activeItems}</strong><em>ONGOING</em></h1>
<h1><strong>${profileData.pendingItems}</strong><em>PENDING</em></h1>
`}
