$(document).ready(function() {
var token = localStorage.getItem("accessToken");
// console.log(token);

if (token === null) {
console.log('not logged in');
} else {
window.location = "/profile.html"
}
});



function logMeIn() {
//getting value from loginistration form
var loginID = document.getElementById('loginMail').value;
var loginPass = document.getElementById('loginPass').value;

console.log(loginID);
console.log(loginPass);

var data = JSON.stringify({
"email": loginID,
"pass": loginPass
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = false;

xhr.addEventListener("readystatechange", function () {
if (this.readyState === 4) {
console.log(this.responseText);
var loginData = JSON.parse(this.responseText);  //parse response from GET as JSON

if (loginData.status == "success") {              //check if loginData has an error or not
localStorage.setItem("accessToken", loginData.response.token)   //store userToken in session
localStorage.setItem("user_id", loginData.response.user_id)   //store userToken in session
localStorage.setItem("identifier", loginData.response.id._id)   //store userToken in session
var token = localStorage.getItem("accessToken");
var uid = localStorage.getItem("user_id");
var identifier = localStorage.getItem("identifier");

const toast = swal.mixin({
toast: true,
position: 'top-end',
showConfirmButton: false,
timer: 3000
});

toast({
type: 'success',
title: 'Signed In successfully'
})


setTimeout(function () {
// Sets the new location of the current window.
window.location = "/profile.html";
}, 4000);

} else {
var loginData = JSON.parse(this.responseText);  //parse response from GET as JSON

                             //no errors on POST or GET
const toast = swal.mixin({
toast: true,
position: 'top-end',
showConfirmButton: false,
timer: 3000
});

toast({
type: 'error',
title: loginData.message
})
}

}
});

xhr.open("POST", "https://cexpress.bubbleapps.io/version-test/api/1.1/wf/login");
xhr.setRequestHeader("content-type", "application/json");
// xhr.setRequestHeader("cache-control", "no-cache");
// xhr.setRequestHeader("postman-token", "dd97bad9-61a7-eebd-edb2-d8c238f9c9c6");

xhr.send(data);
}
