$(document).ready(function() {
var token = localStorage.getItem("accessToken");
// console.log(token);

if (token === null) {
console.log('not logged in');
} else {
window.location = "/profile.html"
}
});

function userSignup() {
//getting value from Registration form
  var regID = document.getElementById('emailInput').value;
  var regPass = document.getElementById('passInput').value;

console.log(regID);
console.log(regPass);

  var data = JSON.stringify({
  "email": regID,
  "pass": regPass
});

var xhr = new XMLHttpRequest();
xhr.withCredentials = false;

xhr.addEventListener("readystatechange", function () {
  if (this.readyState === 4) {
  console.log(this.responseText);
  var regData = JSON.parse(this.responseText);  //parse response from GET as JSON

if (regData.status == "success") {              //check if regData has an error or not
  console.log(regData);
  localStorage.setItem("accessToken", regData.id)   //store userToken in session
  var token = localStorage.getItem("accessToken");
  const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

toast({
  type: 'success',
  title: 'Signed up successfully'
})


setTimeout(function () {
  // Sets the new location of the current window.
  window.location = "/index-login.html";
}, 4000);

} else {
  var regData = JSON.parse(this.responseText);  //parse response from GET as JSON

                               //no errors on POST or GET
  const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

toast({
  type: 'error',
  title: regData.message
})
}

  }
});

xhr.open("POST", "https://cexpress.bubbleapps.io/version-test/api/1.1/wf/signup");
xhr.setRequestHeader("content-type", "application/json");
// xhr.setRequestHeader("cache-control", "no-cache");
// xhr.setRequestHeader("postman-token", "dd97bad9-61a7-eebd-edb2-d8c238f9c9c6");

xhr.send(data);
}
