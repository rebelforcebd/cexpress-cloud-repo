//check for login
var package = localStorage.getItem("accessToken");
if (package === null) {
  window.location = "./index.html"
} else {
  //go along your merry way
}


document.getElementsByTagName('nav')[0].innerHTML = `<nav class="navbar horizontal-layout col-lg-12 col-12 p-0">
  <div class="nav-top flex-grow-1">
    <div class="container d-flex flex-row h-100">
      <div class="text-center navbar-brand-wrapper d-flex align-items-center">
        <a class="navbar-brand brand-logo" href="index.html"><img src="images/preload-logo.png" alt="logo"/></a>
        <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/preload-logo.png" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-stretch justify-content-between flex-grow-1">
        <form class="search-field d-none d-md-flex" action="#">

        </form>
        <ul class="navbar-nav navbar-nav-right mr-0 ml-auto">


          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="icon-bell"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item py-3">
                <p class="mb-0 font-weight-medium float-left">You have 4 new notifications
                </p>
                <span class="badge badge-pill badge-info float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="icon-exclamation mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal text-light mb-1">Application Error</h6>
                  <p class="font-weight-light small-text mb-0">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="icon-bubble mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal text-light mb-1">Settings</h6>
                  <p class="font-weight-light small-text mb-0">
                    Private message
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="icon-user-following mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-normal text-light mb-1">New user registration</h6>
                  <p class="font-weight-light small-text mb-0">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item nav-profile dropdown">
            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
              <!-- <img src="../../local-storage/images/face4.jpg" alt="profile"/> -->
              <span class="nav-profile-name">cExpress Admin</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
              <a class="dropdown-item">
                <i class="icon-settings text-primary mr-2"></i>
                Settings
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item">
                <i class="icon-logout text-primary mr-2"></i>
                Logout
              </a>
            </div>
          </li>
        </ul>
        <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu text-light"></span>
        </button>
      </div>
    </div>
  </div>

  <!-- Menubar -->
  <div class="nav-bottom">
    <div class="container">
      <ul class="nav page-navigation">
        <li class="nav-item">
          <a href="dashboard.html" class="nav-link"><i class="link-icon icon-screen-desktop"></i><span class="menu-title">Dashboard</span></a>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Manage Users</span><i class="menu-arrow"></i></a>
          <div class="submenu">
            <ul class="submenu-item">
              <li class="nav-item"><a class="nav-link" href="adduser.html">Add New User</a></li>
              <li class="nav-item"><a class="nav-link" href="manageusers.html">User List</a></li>
            </ul>
          </div>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Manage Company</span><i class="menu-arrow"></i></a>
          <div class="submenu">
            <ul class="submenu-item">
              <li class="nav-item"><a class="nav-link" href="addcompany.html">New Company</a></li>
              <li class="nav-item"><a class="nav-link" href="companymanage.html">Manage Company</a></li>
              <li class="nav-item"><a class="nav-link" href="manageriders.html">Manage Rider</a></li>
            </ul>
          </div>
        </li>

        <li class="nav-item">
          <a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Details</span><i class="menu-arrow"></i></a>
          <div class="submenu">
            <ul class="submenu-item">
              <li class="nav-item"><a class="nav-link" href="transactionhistory.html">Transaction History</a></li>
              <li class="nav-item"><a class="nav-link" href="orderhistory.html">Order History</a></li>

            </ul>
          </div>
        </li>

      </ul>
    </div>
  </div>
  <!-- menubarend -->

</nav>`
