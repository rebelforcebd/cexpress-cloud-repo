console.log('linked');
const baseURL = 'http://localhost:8080/api/';
const orderDiv = document.getElementById('orderTable');

window.onload= function() {
  //get all orders
  superagent.get(`${baseURL}orders?filter[include]=customer`)
  .then(res => {
    // console.log(res.text);
    var orderData = JSON.parse(res.text);
    // console.log(orderData[0].customer.name);
    orderDiv.innerHTML = orderData.map(orderTemplate).join(" ");
  })
  .catch(err => {
      //console.log(err);
   });
}

function orderTemplate(order, index) {
  //console.log(order);
return `
<tr>
    <td>${index + 1}</td>
    <td>19/10/2018</td>
    <td>20/10/2018</td>
    <td></td>
    <td>${order.recieverName}</td>
    <td>${order.deliveryAddressString}</td>
    <td>${order.deliveryWindow}</td>
    <td>${order.totalDeliveryCost}</td>
    <td>${order.shippingCost}</td>
    <td>
      <label class="badge badge-success">${order.status}</label>
    </td>
    <td>
      <div class="text-center">
        <button type="button" class="btn btn-outline-primary btn-lg" data-toggle="modal" data-target="#ordermodal">View</button>
      </div>
    </td>
</tr>
`

}
//post all orders
aa
