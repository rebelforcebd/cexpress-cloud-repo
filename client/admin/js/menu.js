document.getElementsByClassName('nav-bottom').innerHTML = function() {
return
`<div class="nav-bottom">
        <div class="container">
          <ul class="nav page-navigation">
            <li class="nav-item">
              <a href="index.html" class="nav-link"><i class="link-icon icon-screen-desktop"></i><span class="menu-title">Dashboard</span></a>
            </li>

            <li class="nav-item">
              <a href="/admin/orders className="html"" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Orders</span><i class="menu-arrow"></i></a>
              <div class="submenu">
                <ul class="submenu-item">
                  <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Make Order</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Place Order</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Bulk Order</a></li>
                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Admin</span><i class="menu-arrow"></i></a>
              <div class="submenu">
                <ul class="submenu-item">
                  <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">User Access</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Manage Order</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Manage User Account</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Manage Company Account</a></li>
                </ul>
              </div>
            </li>

            <li class="nav-item">
              <a href="#" class="nav-link"><i class="link-icon icon-book-open"></i><span class="menu-title">Accounts</span><i class="menu-arrow"></i></a>
              <div class="submenu">
                <ul class="submenu-item">
                  <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">My Transaction</a></li>
                  <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Payment</a></li>

                </ul>
              </div>
            </li>

          </ul>
        </div>
      </div>
`
}
