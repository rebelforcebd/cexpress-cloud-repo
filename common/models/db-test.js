'use strict';

module.exports = function(Dbtest) {

    //register custom endpoint
    Dbtest.remoteMethod('testMethod',{
      accepts: {arg: 'testPoint', type:'string', required: true},
      http: {path: '/run-test/:testPoint', verb: 'get'},
      returns: {arg: 'testResponse', type: 'object'}
    });
    //apply logic to custom endpoint
    Dbtest.testMethod = function(testPoint, callback) {
      var testResponse = `Test ${testPoint} has been run successfully`;
      callback(null, testResponse);
    }
};
